﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplicationPOOClassII
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Perro perro1 = new Perro();
            perro1.nombre = "Pepe";
            perro1.raza = "Pastor Aleman";
            perro1.altura = 23.6;

            txtResultado.Text = perro1.comer("Carne");

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Perro perro2 = new Perro();
            perro2.nombre = "Juan";
            perro2.raza = "Mastin";
            perro2.altura = 26;

            txtResultado.Text = perro2.comer("Pescado");
        }
    }
}
