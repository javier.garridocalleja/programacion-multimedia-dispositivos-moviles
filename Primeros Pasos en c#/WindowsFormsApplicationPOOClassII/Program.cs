﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplicationPOOClassII
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
    public class Perro
    {
        // Atributos
        public String nombre;
        public String raza;
        public double altura;

        // Operaciones
        public String comer(String carne)
        {
            return this.nombre + " mide " + this.altura + " y va a comer " + carne;
        }
        public void dormir()
        {

        }

        public void ladrar()
        {

        }
    }
}
