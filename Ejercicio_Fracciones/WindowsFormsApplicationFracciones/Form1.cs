﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplicationFracciones
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Fraccion f1 = new Fraccion(16, 3);
            Fraccion f2 = new Fraccion(10, 10);
            Fraccion f3 = new Fraccion(6, 5);
            Fraccion f4 = new Fraccion(13, 7);
            Fraccion f5 = new Fraccion(12, 6);
            Fraccion f6 = new Fraccion(2, 6);

            f1.Multiplicar(f2);
            label1.Text = "f1 " + f1.Cadena() + "\n";
            f3.Dividir(f4);
            label1.Text += "f3 " + f3.Cadena() + "\n";
            f5.Simplificar();
            label1.Text += "f5 simplificada " + f5.Cadena() + "\n";
            f6.Elevar(Int32.Parse(textBox1.Text));
            label1.Text += "f3 elevado a " + Int32.Parse(textBox1.Text) + f6.Cadena() + "\n"; 

        }
    }
}
