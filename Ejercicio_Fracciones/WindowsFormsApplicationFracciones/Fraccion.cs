﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplicationFracciones
{
    class Fraccion
    {
        private int numerador, denominador;

        // constructor
        public Fraccion (int n)
        {
            numerador = n;
        }
        public Fraccion (int n, int d)
        {
            numerador = n;
            denominador = d;
        }
        
        // funcion multiplicar
        public void Multiplicar (Fraccion f)
        {
            numerador = numerador * f.numerador;
            denominador = denominador * f.denominador;
        }

        // funcion dividir
        public void Dividir (Fraccion f)
        {
            numerador = numerador * f.denominador;
            denominador = denominador * f.numerador;
        }

        // funcion simplificar
        public void Simplificar ()
        {
            bool encontrado = false;
            for( int i = 2; (i<=numerador) && (encontrado == false); i++)
            {
                if( ((numerador%i) == 0) && ((denominador%i)==0) ) {
                    numerador = numerador / i;
                    denominador = denominador / i;
                    // si queremos reducir una vez encontrado lo igualamos a true y 
                    // si la queremos hacer irreducible encontrado lo igualamos a false
                    encontrado = true;
            }
            }
        }

        // funcion Elevar
        public void Elevar (int e)
        {
            int vn = 1, vd = 1;
            for (int i=1; i<=e; i++)
            {
                vn = vn * numerador;
                vd = vd * denominador;
            }
            vn = numerador;
            vd = denominador;
        }

        public string Cadena()
        {
            return numerador + " / " + denominador;
        }
    }
}
