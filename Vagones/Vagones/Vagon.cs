﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vagones
{
    class Vagon
    {
        private int nOcupantes, pesoMaximo;
        private bool permiteEnganche, unido;

		public Vagon(int nOcu, int pMax, bool perm)
        {
            nOcupantes = nOcu;
            pesoMaximo = pMax;
            permiteEnganche = perm;
            unido = false;
        }

		public bool Unir(Vagon v)
        {
            if (permiteEnganche == true)
            {
                unido = true;
                nOcupantes += v.nOcupantes;
                pesoMaximo += v.pesoMaximo;
                return true;
            }
            else
                return false;
        }

		public void Dividir ()
        {
			if(unido == true)
            {
                unido = false;
                nOcupantes = nOcupantes / 2;
                pesoMaximo = pesoMaximo / 2;
            }
        }

		public string Mostrar()
        {
            return "El numero de ocupantes es " + nOcupantes +
                    " El peso máximo es: " + pesoMaximo +
                    " Permite enganche: " + permiteEnganche;
        }

	}
}
