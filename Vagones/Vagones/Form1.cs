﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vagones
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonCrear_Click(object sender, EventArgs e)
        {
            Vagon v1, v2, v3;
            int nOc = 20, peso = 200;
            
            v1 = new Vagon(Int32.Parse(textBoxOcupantes.Text), Int32.Parse(textBoxPeso.Text), true);
            v2 = new Vagon(nOc, peso, false);
            v3 = new Vagon(nOc+10, peso+100, false);

            v1.Unir(v2);
            v1.Unir(v3);

            labelResultado.Text = "";
            labelResultado.Text += v1.Mostrar() + "\n";
            v1.Dividir();
            labelResultado.Text += v1.Mostrar() + "\n";

            if (v2.Unir(v3) == true)
                labelResultado.Text += "Union de v2 y v3 realizada correctamente";
            else
                labelResultado.Text += "Union de v2 y v3 no realizada";
        }
    }
}
