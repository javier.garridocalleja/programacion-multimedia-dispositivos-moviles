﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RotarTabla
{
    public partial class Form1 : Form
    {
        string[] nombres;
        public Form1()
        {
            InitializeComponent();
            nombres = new string[6];
            nombres[0] = "AA";
            nombres[1] = "BB";
            nombres[2] = "CC";
            nombres[3] = "DD";
            nombres[4] = "EE";
            nombres[5] = "FF";
            MostrarTabla(nombres);

        }
        private void RotarDerecha(string[] tabla)
        {
            string ultimaposicion;
            ultimaposicion = tabla[tabla.Length - 1];
            for(int i=tabla.Length-1; i>0; i--)
            {
                tabla[i] = tabla[i - 1];
            }
            tabla[0] = ultimaposicion;
        }
        private void MostrarTabla(string[] tabla)
        {
            labelResultado.Text = "";
            for (int i = 0; i < tabla.Length; i++)
                labelResultado.Text += tabla[i] + " - ";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RotarDerecha(nombres);
            MostrarTabla(nombres);
        }
    }
}
